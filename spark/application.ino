// This #include statement was automatically added by the Spark IDE.
#include "LiquidCrystal.h"


LiquidCrystal lcd(D0, D1, D2, D3, D4, D5);

const int contrastPin = A0;
String twitterHandle;
String tweet;
int tweetLen;
int tweetStartPos;
int tweetEndPos;

void setup() {
  Spark.function("contrast", setContrast);
  Spark.function("setTwitterHandle", setTwitterHandle);
  Spark.function("setTweet", setTweet);
  pinMode(contrastPin, OUTPUT);

  analogWrite(contrastPin, 10);
  lcd.begin(16, 2);

  twitterHandle = "@jdztwitspark";
  tweet = "Tweet me!";

  setLcdRows();
}

void loop() {
  //row 0 will remain static, row 1 will appear to "marquee"
  lcd.clear();

  lcd.setCursor(0, 0);
  lcd.print(twitterHandle);

  lcd.setCursor(0, 1);

  if (tweetLen <= 16) {
    lcd.print(tweet);
  }

  else {
    String displayTweet = tweet.substring(tweetStartPos, tweetEndPos);
    if (tweetEndPos < tweetStartPos) {
      displayTweet =
        tweet.substring(tweetStartPos, tweetLen - 1) + " "
          + tweet.substring(0, tweetEndPos - 1);
    }
    lcd.print(displayTweet);
    tweetStartPos = tweetStartPos > tweetLen ? 0 : tweetStartPos++;
    tweetEndPos = tweetEndPos > tweetLen ? 0 : tweetEndPos++;
  }

  delay(1000);
}

int setContrast(String cmd) {
  int contrastVal = cmd.toInt();
  if (contrastVal == NULL) return -1;

  analogWrite(contrastPin, contrastVal);
  return 1;
}

int setTwitterHandle(String cmd) {
  if (cmd == NULL || cmd.length() < 1) return -1;

  twitterHandle = cmd;
  return 1;
}

int setTweet(String cmd) {
  if (cmd == NULL || cmd.length() < 1) return -1;

  String handleTemp = cmd.substring(0, cmd.indexOf(" "));
  twitterHandle = handleTemp;

  String tweetTemp = cmd.substring(cmd.indexOf(" ") + 1, cmd.length());
  tweet = tweetTemp;
  tweetLen = cmd.length();
  tweetStartPos = 0;
  tweetEndPos = tweetLen <= 16 ? tweetLen - 1 : 15;
  return 1;
}

void setLcdRows() {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(twitterHandle);
  lcd.setCursor(0, 1);
  lcd.print(tweet);
}
