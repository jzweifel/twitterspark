var settings = require('./settings.js');
var controller = require('./lib/controller.js');
var Twit = require('twit');

//create stream
//var T = new Twit(settings.user_stream);

var T = new Twit({
    consumer_key:         settings.user_stream.consumer_key    			// add your twitter consumer key here
  , consumer_secret:      settings.user_stream.consumer_secret    		// add your twitter consumer secret here
  , access_token:         settings.user_stream.access_token     		// add your twitter access token key here
  , access_token_secret:  settings.user_stream.access_token_secret     	// add your twitter access token secret here   
})


var stream = T.stream('user', { track: "settings.twitter_user"});


var commands = {
    "setTweet": controller.setTweet
}


//listen stream data
stream.on('tweet', function(json) {
    var text = json.text;
    console.log("heard " + text);
	
	if (text.indexOf(settings.twitter_user) > -1)
	{	
		twitterHandle = "@" + json.user.screen_name;
		tweet = twitterHandle + " " + text.substring(text.indexOf(' ') + 1, text.length);
		
		console.log("running command setTweet with args " + tweet);
		commands["setTweet"](tweet);  
	}
});