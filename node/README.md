Spark Twitter Magnet
==============

Display Tweets on the tiny LCD !

### Setting Up:
 1. Sign up/ Register into you Twitter account at: https://dev.twitter.com/
 2. Create a new app and generate the keys/ tokens (make sure your app has read/write access)
 3. Rename settings.js.example to settings.js and update file with your twitter credentials and spark credentials
 4. Make sure you have node installed on your machine
 5. run `npm install` followed by `npm start`
 6. run the main file `node main.js` (make sure your core is connected to the cloud before running this file)
 7. You are now ready to receive messages on your twitter magnet!

For a tweet to show up on the LCD screen, simply tweet @yourusername hello <-- replace hello with your message. For example: @sparkdevices hello world!
