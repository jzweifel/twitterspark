var settings = require('../settings.js');
var ApiClient = require('./ApiClient.js');

var client = new ApiClient(settings.apiUrl);
client.login(settings.coreuser, settings.corepassword);


var that = {
    setTweet: function (arg) {
        that.dispCmd("setTweet", arg);
    },
    dispCmd: function (func, arg) {
        client.callFunction(settings.coreID, func, arg);
    }
};
module.exports = that;